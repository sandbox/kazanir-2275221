<?php



// Our default plugin.

$plugin = array(
  'title' => 'Default',
  'class' => 'CommerceSgPluginDefault',
);

// The implements declaration is necessary for now since I haven't worked out how the interface and abstract class will relate to one another.

class CommerceSgPluginDefault extends CommerceSgPluginBase implements CommerceSgPluginInterface { 
  
  public function form(&$form, &$form_state) {
    $settings_form = array(
      '#prefix' => '<div id="commerce-sg-plugin-settings-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,      
      '#type' => 'container',
    );
    
    return $settings_form;
  }
  
  public function formValidate(&$form, &$form_state) {
    
  }
  
  public function computeMembers($sg) {
    return array_map(function($item) { return $item['target_id']; }, $sg->commerce_sg_users[LANGUAGE_NONE]);
  }
  
  public function showUsersField() {
    return TRUE;
  }
  
}