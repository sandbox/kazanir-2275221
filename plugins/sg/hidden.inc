<?php



// Our default plugin.

$plugin = array(
  'title' => 'Hidden',
  'class' => 'CommerceSgPluginHidden',
);

// Tthis "hidden" class sucks and is only here as a demo of how to remove the users field. Since it replaces it with nothing it shouldn't be used!

class CommerceSgPluginHidden extends CommerceSgPluginBase implements CommerceSgPluginInterface { 
  
  public function form(&$form, &$form_state) {
    $settings_form = array(
      '#prefix' => '<div id="commerce-sg-plugin-settings-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,      
      '#type' => 'container',
    );
    
    return $settings_form;
  }
  
  public function formValidate(&$form, &$form_state) {
    
  }
  
  public function computeMembers($sg) {
    return array_map(function($item) { return $item['target_id']; }, $sg->commerce_sg_users[LANGUAGE_NONE]);
  }
  
  public function showUsersField() {
    return FALSE;
  }
  
}