<?php

// Upload a CSV file that maps a specific column to a user attribute and assigns those users.

$plugin = array(
  'title' => 'CSV Upload',
  'class' => 'CommerceSgPluginUpload',
);

// The implements declaration is necessary for now since I haven't worked out how the interface and abstract class will relate to one another.

class CommerceSgPluginUpload extends CommerceSgPluginBase implements CommerceSgPluginInterface {

  public $plugin_name = 'upload';
  
  public function form(&$form, &$form_state) {
    $settings_form = array(
      '#prefix' => '<div id="commerce-sg-plugin-settings-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,      
      '#type' => 'fieldset',
    );
    $settings_form['file'] = array(
      '#type' => 'file',
      '#title' => t('CSV File'),
      '#description' => t('Upload a file with user data. Select the column of the CSV and its matching user property or field below.'),
      '#name' => 'files[sg_plugin_upload_file]',
    );
    $settings_form['column_heading'] = array(
      '#type' => 'textfield',
      '#title' => t('Column Heading'),
      '#description' => t('Enter the heading of the column to search for user identifiers.'),
      '#default_value' => (isset($this->settings['column_heading']) ? $this->settings['column_heading'] : ''),
    );
    $settings_form['user_property'] = array(
      '#type' => 'select',
      '#title' => t('User Property'),
      '#description' => t('Pick the user property or field the uploaded file contains.'),
      '#default_value' => (isset($this->settings['user_property']) ? $this->settings['user_property'] : ''),
      '#options' => $this->userPropertyList(),
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'commerce_sg_plugin_ajax_dynamic',
        'parameters' => array(
          'plugin' => $this->plugin_name, 
          'callback' => 'commerce_sg_plugin_upload_ajax_refresh'
        ),
      ),
    );    
    $user_field_cols_list = $this->userFieldColumns($this->settings['user_property']);
    $settings_form['user_field_column'] = array(
      '#title' => t('Sub-field'),
      '#description' => t('Pick the value of this field to compare against.'),
      '#default_value' => (isset($this->settings['user_field_column']) ? $this->settings['user_field_column'] : ''),
      '#prefix' => '<div id="commerce-sg-upload-user-field-column">',
      '#options' => ($user_field_cols_list ? $user_field_cols_list : NULL),
      '#suffix' => '</div>',
      '#type' => ((bool) $user_field_cols_list ? 'select' : 'hidden'),  
    );
    
    
    
    return $settings_form;
  }
  
  public function formValidate(&$form, &$form_state) {
    $file = file_save_upload('sg_plugin_upload_file', array('file_validate_extensions' => array('csv')));
    $settings = &$form_state['values']['settings'];
    if ($file) {
      $lines = file($file->uri);
      $user_upload_values = array();
      foreach ($lines as $row => $line) {
        if ($row == 0) {
          $heading = $settings['column_heading'];
          $key = array_search($heading, str_getcsv($line));
          if ($key === FALSE) {
            form_set_error('settings][file', t("The provided column heading could not be located in your CSV file."));
            $skip_processing_error = TRUE;
            return;
          }
        }
        else {
          $columns = str_getcsv($line);
          $user_upload_values[] = $columns[$key];
        }
      }
      $this->form_state =& $form_state;
      $this->user_upload_values = $user_upload_values;
      
      $uid_results = $this->computeUsers($this->sg);
      
      // Finally we should have a $uid_results we can map onto the field values in $form_state.
      $user_reference_field_items = array_map(function($uid) { return array('target_id' => $uid); }, $uid_results);
      $form_state['values']['commerce_sg_users'][LANGUAGE_NONE] = $user_reference_field_items;
    }
    else {
      form_set_error('settings][file', t("The uploaded CSV file failed to save or be parsed properly. Please try again."));
    }
  }
  
  public function showUsersField() {
    return FALSE;
  }
  
  public function computeMembers($sg) {
    $settings = $this->settings;
    if (isset($this->form_state)) {
      $form_state =& $this->form_state;
      $settings += $form_state['values']['settings'];
    }
    $user_upload_values = $this->user_upload_values;
    
    // Before we do our own logic, we give other modules the attempt to
    //  intervene. This is so someone else can use this plugin for non-
    //  standard user fields or properties without re-writing the upload
    //  functionality.
    $uid_results = array();
    foreach (module_implements('commerce_sg_upload_user_results') as $module) {
      if (empty($uid_results)) {
        // Note that form_state is passed here just in case one needs settings
        //  and cannot be altered within module_invoke.
        $uid_results = module_invoke($module, 'commerce_sg_upload_user_results', $user_upload_values, $form_state);
        // In case someone sucks at implementing this hook we reset all empty values to be an array.
        if (empty($uid_results)) {
          $uid_results = array();
        }
        // If $uid_results is filled in, no other hooks will be called -- 
        //  only the first implementing module will win.
      }
    }
    if (empty($uid_results)) {
      // Having assembled our values, let's EFQ.
      $property_name = $settings['user_property'];
      $props = $this->userPropertyInfo();
      $prop = $props[$property_name];

      $efq = new EntityFieldQuery();
      $efq->entityCondition('entity_type','user');
      if (@$prop['field']) {
        // Bunch of logic here for multi-column fields.
        $column_name = 'value';
        if (isset($prop['property info'])) {
          if (isset($settings['user_field_column']) && in_array($settings['user_field_column'], array_keys($prop['property info']))) {
            // Okay. Our field schema column is both set and still defined as a sub-property of the chosen field. Yay.
            $column_name = $settings['user_field_column'];
            $efq->fieldCondition($settings['user_property'], $column_name, $user_upload_values, 'IN');
            $results = $efq->execute();
            if (array_key_exists('user', $results)) {
              $uid_results = array_keys($results['user']);
            }
          }
          else {
            form_set_error('settings][file', t("You have chosen an invalid sub-field."));
          }
        }
        else {
          $efq->fieldCondition($settings['user_property'], $column_name, $user_upload_values, 'IN');
          $results = $efq->execute();
          if (array_key_exists('user', $results)) {
            $uid_results = array_keys($results['user']);
          }
        }
      }
      elseif(@$prop['schema field']) {
        // This is a simple property.
        $schema_field = $prop['schema field'];
        $efq->propertyCondition($schema_field, $user_upload_values, 'IN');
        $results = $efq->execute();
        $uid_results = array_keys($results['user']);
      }
      elseif(@$prop['commerce_sg_override']) {
        // Do nothing here. The results alter should handle this if we 
        // somehow made it to this point without any results.
      }
      else {
        // Something is messed up and the chosen property can't be evaluated. Return nada and set an error.
        form_set_error('settings][file', t("The query for users based on your chosen column couldn't be run successfully and no users have been added."));
      }
    }
    // We give modules another chance to alter these results.
    drupal_alter('commerce_sg_upload_user_results', $uid_results, $form_state);
    
    return $uid_results;
  }
  
  protected function userPropertyInfo() {
    $info = entity_get_property_info('user');
    $fields = isset($info['bundles']['user']['properties']) ? $info['bundles']['user']['properties'] : array();
    $props = $info['properties'] + $fields;
    // Allow other modules to alter the list of choosable user properties, 
    //  in case they have custom logic or an EFQ extension that can handle 
    //  more than fields/schema properties. If this is done and the "field"
    //  or "schema field" keys are not present, the alter hook should set
    //  the "commerce_sg_override" key to TRUE. See the ->userPropertyList
    //  method which generates the options form select list.
    drupal_alter('commerce_sg_upload_user_properties', $props);
    return $props;
  }
  
  protected function userPropertyList() {
    $info = $this->userPropertyInfo();
    $filtered = array_filter($info, function($prop) { return (@$prop['field'] || @$prop['schema field'] || @$prop['commerce_sg_override']); });
    array_walk($filtered, function(&$prop, $key) { $prop = $prop['label']; });
    return $filtered;
  }
  
  protected function userFieldColumns($property_name) {
    $info = $this->userPropertyInfo();
    if (isset($info[$property_name]['property info'])) {
      $prop = $info[$property_name]['property info'];
      array_walk($prop, function (&$sub, $key) { $sub = $sub['label']; });
      return $prop;
    }
    else {
      return FALSE;
    } 
  }
  
  
}

function commerce_sg_plugin_upload_ajax_refresh($form, $form_state) {
  $stuff = array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace('#commerce-sg-upload-user-field-column', drupal_render($form['settings']['user_field_column'])),
    ),
  );
  
  return $stuff;
}