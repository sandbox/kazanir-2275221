<?php

$plugin = array(
  'title' => 'PHP Code',
  'class' => 'CommerceSgPluginPhp',
  'sync' => 'dynamic',
);

class CommerceSgPluginPhp extends CommerceSgPluginBase {

  public $plugin_name = 'php';
  
  public function form(&$form, &$form_state) {
    $settings_form = array(
      '#prefix' => '<div id="commerce-sg-plugin-settings-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,      
      '#type' => 'fieldset',
    );
    $settings_form['php_code'] = array(
      '#type' => 'textarea',
      '#title' => t('PHP Code'),
      '#description' => t('Enter PHP code which will be evaluated. The return value should be an array of Drupal user UIDs. Printed output will not be rendered.'),
    );
    
    return $settings_form;
  }

  public function formValidate(&$form, &$form_state) {
    $this->form_state =& $form_state;
    
    $uid_results = $this->computeMembers();
      
    // Finally we should have a $uid_results we can map onto the field values in $form_state.
    $user_reference_field_items = array_map(function($uid) { return array('target_id' => $uid); }, $uid_results);
    $form_state['values']['commerce_sg_users'][LANGUAGE_NONE] = $user_reference_field_items;
  }
  
  public function computeMembers($sg = NULL) {
    if (isset($this->form_state['settings']['php_code'])) {
      $code = $this->form_state['settings']['php_code'];
    }
    else {
      $code = $this->settings['php_code'];
    }
    
    if (!empty($code)) {
      $func = create_function('', $code);
      ob_start();
      $uid_results = $func();
      ob_get_clean();
    }
    else {
      $uid_results = array();
    }
    return $uid_results;
  }
  
  public function showUsersField() {
    return FALSE;
  }
  
}