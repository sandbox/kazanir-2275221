<?php

function commerce_sg_schema() {

  $schema['commerce_sg'] = array(
    'description' => 'Stores information about defined shopper groups.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique shopper group identifier.',
      ),
      'name' => array(
        'description' => 'The machine-readable name of this shopper group.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this shopper group.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'The weight of this shopper group in relation to others.',
      ),
      'sync' => array(
        'description' => 'The syncing setting (dynamic or static) of this SG.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'plugin' => array(
        'description' => 'The machine name of the CTools plugin this SG relies on for settings.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of additional data related to this shopper group.',
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );





  return $schema;
}

/**
 * Add the SG entity table properly!
 */
function commerce_sg_update_7100() {
  $schema = commerce_sg_schema();
  db_create_table('commerce_sg', $schema['commerce_sg']);
  
  return "Added shopper group entity table.";
}